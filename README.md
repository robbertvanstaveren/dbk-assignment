Bijenkorf Assignment
------

### Deliverables

The deliverables for this assignment are a zip containing
    
- a static HTML template
- supporting resources (CSS / JavaScript / Images etc)
- supporting documentation

The static HTML template should
    
+ have a semantic markup
+ have a responsive layout
+ Include micro­data (for more information see the following URL: h​ttp://schema.org/)​

Each front end layer (CSS / JS / HTML) should

+ have a clearly defined structure
+ be written with re­usability in mind
+ be well documented
+ degrade gracefully in older browsers

The supporting documentation should contain:

+ A brief explanation of the approach used
+ Reasons for using certain web technologies / patterns
+ Any questions or observations that you may have

### Support

* IE8+
* Firefox 28 +
* Chrome 30 +
* Safari 7 +
* Opera 18 +
* Mobile Safari (iOS 5 +)
* Android Browser (Android 2.3 +)

Approach Used
------
After reading the assignment I identified elements that could take up the most time:

* the slider on mouseover
* adding microdata in the correct way (i've never used it before)
* responsiveness
* IE8 compatibility

Based on the allowed time I decided to use:

- Bootstrap (Grid)
- Slick (carousel/slider)

**Bootstrap** helps me with setting up the desired html layout quickly and creates a base for implementing responsive behaviour.

**Slick** is a responsive carousel which is easy to use. It has breakpoint capabilities which allows me to change its behaviour based on the breakpoint we are in.

I've created a small js file which holds all the functions for this demo page

#### What I want to get done in the allowed time
- add the color slider for desktop
    + remove the slider for mobile and change it to a fixed element
    + if time allows; create a different mobile view which emphasizes important things for mobile users (images, prices and color/size variations)
- add microdata for all important elements
- complete the assignment to my satisfaction

Assignment Outcome / Observations
------
First of all, I've never had to do a assignment like this for a job application so I was kind of exited when I started on the assignment. In a normal
working situation I would have probably created the same code, but working on a assignment like this makes you think about things a little more.

I've kept everything as simple as possible; I've not included any build tools or minifiers

The things i spent the most time on:

- getting the slider to look and behave normal (hidden sliders can not get height information so had to change my css a bit)
- responsiveness 
- finding the correct microdata for certain elements

I decided to show the overlay without any animations. I could have faded the overlay information (in <-> out) but I as a user would not like that
very much because I do not want to wait for the information to show. An animation can be nice for a couple of times but not while
browsing a lot of items.

A note on the slider; this will not show if the page is viewed with a touch device.

All in all I must have spent a little over 4 hours. Things I would like to have done but did not have the time for:

- extended mobile view: I wanted to create a more rich view for mobile visitors. I decided to not show the color slider on mobile, I wanted to
make this a fixed element which is always visible on mobile (due to hovering not working on touch). I decided not to do this because I would
have gone over the 4 hours by at least 30+ minutes.

- I did not implement all the font styles as they are on bijenkorf.nl. I wanted to copy parts of the bijenkorf.nl CSS (including the icon font) but decided
not to because I didn't know if it was allowed.

- I could not find dress images in all the different colors so I used colored placeholders for these.

- I had time to do some tests in IE8/IE9/IE10/IE11, I have those virtual machines available so that was easier than the other older browsers.
I checked the newest Firefox/Chrome and Safari with no issues. I did not check the older Firefox/Chrome/Safari and Opera due to time issues.

As far as this assignment goes; If this was a dynamic overview of products, I would have probably used AngularJS for the templating and getting
of the data. Because the assignment was for a static page, I decided using a framework was too much.

I had fun doing the assignment.


### Robbert








