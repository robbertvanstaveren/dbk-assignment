/**
 * @namespace BK
 *
 */
/*global BKAPP*/
BK = (function ($,Modernizr) {
    'use strict';

    var config = {};

    return {
        /**
         * Initialize all the needed scripts
         */
        init:function() {
            this.hooks();
            this.createSliders();
        },
        /**
         * Bind all the jquery events
         */
        hooks: function() {
            // only bind if this is not a touch device
            if (!Modernizr.touch) {
                $('li.productlisting__item').hover(
                  function() {
                    $(this).toggleClass('active');
                  }, function() {
                    $(this).toggleClass('active');
                  });
            }

            $('.sizes-list button').on('click touchstart', function(){
                var href = $(this).data('href');
                window.location = href;
            });
        },
        /**
         * Create all the color sliders at once
         */
        createSliders: function() {
            $('.slider').slick({
              dots: false,
              infinite: false,
              speed: 300,
              slidesToShow: 4,
              slidesToScroll: 1,
              vertical: true,
              prevArrow: '<div class="slide-nav uparrow"><i class="fa fa-angle-up fa-2x"></i></div>',
              nextArrow: '<div class="slide-nav downarrow"><i class="fa fa-angle-down fa-2x"></i></div>',
              verticalSwiping: true,
              responsive: [
                {
                  breakpoint: 480,
                  settings: "unslick"
                }
              ]
            });

        }
    };
})(window.$, window.Modernizr);
$(document).ready(function(){BK.init();});
